import AppNavbar from './components/AppNavbar';
import Home from './pages/Home'

import './App.css';


import { Container} from 'react-bootstrap';

function App() {
  return (
    <>
    <AppNavbar />
    <Container>
      <Home />
    </Container >
    </>
  );
}

export default App;



