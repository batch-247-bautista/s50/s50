import Banner from '../components/Banner';
import Highlight from '../components/Highlight';
import CourseCard from '../components/CourseCard';

export default function Home(){
	return (
		<>
			<Banner />
			<Highlight />
			<CourseCard />
		</>
			)
}
