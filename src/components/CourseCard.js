import React from 'react';
import Button from 'react-bootstrap/Button';
import { Card } from 'react-bootstrap';

export default function CourseCard(course) {
  return (
    <Card>
      <Card.Header as="h5">{course.name}</Card.Header>
      <Card.Body>
        <Card.Text><strong>Sample Course</strong></Card.Text>
        <Card.Text>Description:</Card.Text>
        <Card.Text>This is a sample course offering.</Card.Text>
        <Card.Text>Price:</Card.Text>
        <Card.Text>PHP40,000.00</Card.Text>
        <Button variant="primary">Enroll Now!</Button>
      </Card.Body>
    </Card>
  );
};

